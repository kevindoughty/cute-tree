module.exports = {
	extends: ["eslint:recommended", "plugin:@typescript-eslint/recommended"],
	parser: "@typescript-eslint/parser",
	plugins: ["@typescript-eslint"],
	root: true,
	parserOptions: {
        project: ["./tsconfig.json"],
    },
	rules: {
		"indent": ["error", "tab", { "SwitchCase": 1 }],
		"linebreak-style": ["error", "unix"],
		"quotes": ["error", "double"],
		"semi": ["error", "always"],
		"comma-dangle": ["error", "never"],
		"no-cond-assign": ["error", "except-parens"],
		"no-console": "error",
		//"no-unused-vars": "error", // can't override typescript-eslint
		"eqeqeq": 2,
		"no-trailing-spaces": [2, { "skipBlankLines": true }],
		"no-constant-condition": ["error", { "checkLoops": false }]
	}
};