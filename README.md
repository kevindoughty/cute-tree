# cute-tree

Minimum viable tree view example which preserves selection and collapsed/expanded state through undo and redo, implemented as an indented list.

[https://kevindoughty.gitlab.io/cute-tree/index.html](https://kevindoughty.gitlab.io/cute-tree/index.html)