import { h, FunctionComponent } from "preact";
import { Getters, Setters } from "./state.js";
import { UndoManager } from "@kvndy/undo-manager";

type HeaderProps = {
	getters: Getters,
	setters: Setters,
	manager: UndoManager
};
const Header: FunctionComponent<HeaderProps> = ({ getters, setters, manager }: HeaderProps) => {
	const { undo, redo, canUndo, canRedo, undoDescription, redoDescription } = manager;
	const addItem = () => {
		setters.addItem();
	};
	const deleteAndReparent = () => {
		setters.deleteAndReparent();
	};
	const deleteAndAllDescendants = () => {
		setters.deleteAndAllDescendants();
	};
	const deselect = () => {
		setters.deselect();
	};
	return (
		<header>
			<button onClick={ undo } title={ undoDescription.value } disabled={ !canUndo.value }>Undo</button>
			<button onClick={ redo } title={ redoDescription.value } disabled={ !canRedo.value }>Redo</button>
			<button onClick={ addItem }>Add Child</button>
			<button onClick={ deleteAndReparent } disabled={ !getters.hasSelection }>Delete Only</button>
			<button onClick={ deleteAndAllDescendants } disabled={ !getters.hasSelection }>Delete All</button>
			<button onClick={ deselect } disabled={!getters.hasSelection}>Deselect</button>
		</header>
	);
};
export default Header;