import { h, Component } from "preact";
import { useRef } from "preact/hooks";
import { Setters, Id } from "./state.js";

type ListItemProps = {
	id: Id,
	text: string,
	expandable: boolean,
	toggled: boolean,
	selected: boolean,
	depth: number,
	refReporter: (element: HTMLElement) => void,
	setters: Setters
};
class ListItem extends Component<ListItemProps> {
	item: HTMLElement | null = null;
	setRef = (dom: HTMLElement | null) => this.item = dom;

	componentDidMount() {
		const props = this.props;
		if (props && props.selected) {
			if (props.refReporter && this.item !== null) {
				const item: HTMLElement = this.item;
				props.refReporter(item);
			}
		}
	}

	render() {
		const props = this.props;
		const ref = useRef(null);

		const {
			id,
			text,
			expandable,
			toggled,
			selected,
			depth
		} = props;

		const name = String(id); // lol

		const itemStyle = {
			"--depth": depth
		};

		function handleCheckChange() {
			props.setters.disclosureToggle(id);
		}
		function handleCheckClick(e: Event) {
			e.stopPropagation(); // needed to prevent list item click
		}
		function handleClickListItem() {
			props.setters.selectListItem(id);
		}
		function handleBlurText(e: Event) {
			const target = e.target;
			if (target) {
				const text: string = (target as HTMLInputElement).value;
				props.setters.setText(id, text);
			}
		}
		function handleKeyDownText(e: KeyboardEvent) {
			if (e.keyCode === 13) {
				if (ref && ref.current) {
					const element: HTMLElement = ref.current;
					element.blur();
				}
			}
		}

		let rowElement = (
			<span>
				{ text }
			</span>
		);
		if (selected) rowElement = (
			<input
				ref={ref}
				type="text"
				name={name}
				value={text}
				onBlur={handleBlurText}
				onKeyDown={handleKeyDownText}
			/>
		);
		if (!expandable) {
			return (
				<li
					ref={this.setRef}
					key={name}
					className={selected ? "selected" : undefined}
					style={itemStyle}
					onClick={ handleClickListItem }
				>
					{ rowElement }
				</li>
			);
		}
		return (
			<li
				ref={this.setRef}
				key={name}
				className={selected ? "selected" : undefined}
				style={itemStyle}
				onClick={ handleClickListItem }
			>
				<input
					type="checkbox"
					id={name}
					name={name}
					checked={toggled}
					onChange={ handleCheckChange }
					onClick={ handleCheckClick }
				/>
				<div/>
				{rowElement}
			</li>
		);
	}
}

export default ListItem;