import { h, FunctionComponent } from "preact";
import { useRef } from "preact/hooks";
import { effect } from "@preact/signals";
import ListItem from "./ListItem.jsx";

import { Getters, Setters, Id } from "./state.js";

type ListViewProps = {
	getters: Getters,
	setters: Setters,
};
const ListView: FunctionComponent<ListViewProps> = ({getters, setters}: ListViewProps) => {
	const ref = useRef(null);

	effect( () => {
		const value = getters.getScroll();
		if (ref && ref.current) {
			const element: HTMLElement = ref.current;
			if (element.scrollTop !== value) element.scrollTop = value;
		}
	});

	const refReporter = function(element: HTMLElement) {
		element.scrollIntoView();
	};

	const onScroll = function(e: Event) {
		const element = e.target;
		if (element) {
			const scrollTop = (element as HTMLElement).scrollTop;
			setters.setScroll(scrollTop);
		}
	};

	const children = [];
	const length = getters.numberOfItems();
	for (let i=0; i<length; i++) {
		const id: Id = getters.idAtIndex(i);
		const text: string = getters.textAtIndex(i);
		const isExpandable = getters.isExpandableAtIndex(i);
		const isExpanded = getters.isExpandedAtIndex(i);
		const isSelected = getters.isSelectedAtIndex(i);
		const depth = getters.depthAtIndex(i);

		children.push(
			<ListItem
				id={id}
				text={text}
				expandable={isExpandable}
				toggled={isExpanded}
				selected={isSelected}
				depth={depth}
				refReporter={refReporter}
				setters={setters}
			/>
		);
	}
	return (
		<ul
			class="list"
			ref={ref}
			onScroll={ onScroll }
		>
			{ children }
		</ul>
	);
};
export default ListView;