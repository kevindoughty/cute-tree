import { computed, ReadonlySignal } from "@preact/signals";
import { UndoManager, Undoable, Preservable } from "@kvndy/undo-manager";

import { ToWords } from "to-words";
const toWords = new ToWords();

type Id = number;
type Node = {
	text: string,
	childIds: Array<Id>
};
type Tree = Record<Id, Node>
type Model = {
	tree: Undoable<Tree>,
	nextId: Id, // lol
};
type Presentation = {
	allCollapsedIds: Preservable<Array<Id>>,
	selectedId: Preservable<Id>,
	listScroll: Preservable<number>
};
type Derived = {
	flattenedIdsLinear: ReadonlySignal<Array<Id>>,
	flattenedDepthLookup: ReadonlySignal<Array<number>>,
	flattenedVisibleIds: ReadonlySignal<Array<Id>>,
	visibleCollapsedIds: ReadonlySignal<Array<Id>>,
	parentIds: ReadonlySignal<Record<Id, Id>>
};
type Getters = {
	numberOfItems: () => number,
	hasSelection: () => boolean,
	getScroll: () => number,
	idAtIndex: (index: number) => Id,
	depthAtIndex: (index: number) => number,
	textAtIndex: (index: number) => string,
	isExpandableAtIndex: (index: number) => boolean,
	isExpandedAtIndex: (index: number) => boolean,
	isSelectedAtIndex: (index: number) => boolean
}
type Setters = {
	setText: (id: Id, text: string) => void,
	setScroll: (scrollTop: number) => void,
	disclosureToggle: (id: Id) => void,
	addItem: () => void,
	deleteAndReparent: () => void,
	deleteAndAllDescendants: () => void,
	selectListItem: (id: Id) => void,
	deselect: () => void
};
type State = {
	getters: Getters,
	setters: Setters,
	model: Model, // for testing
	presentation: Presentation, // for testing
	derived: Derived // for testing
};


const getNode = function(id: Id, tree: Tree) {
	const key: number = id;
	return tree[key];
};
const setNode = function(node: Node, id: Id, tree: Tree) {
	const key: number = id;
	tree[key] = node;
};
const deleteNode = function(id: Id, tree: Tree) {
	const key: number = id;
	delete tree[key];
};

const textFromId = function(id: Id) {
	const num: number = id;
	return toWords.convert(num).toLowerCase().replace("-", " ").replace(",", "");
};
const incrementId = function(id: Id) { // lol
	const num: number = id;
	return num + 1;
};

const randomTree = function(length: number) : Tree {
	const tree: Tree = Object.create(null);
	if (length < 0) length = 0;
	for (let i=0; i<length+1; i++) {
		const id: Id = i;
		const child: Node = {
			text: textFromId(id),
			childIds: []
		};
		setNode(child, id, tree);
		if (i > 0) {
			const parentId: Id = Math.floor(Math.random() * i);
			tree[parentId].childIds.push(id);
		}
	}
	return tree;
};

const initSetters = function(model: Model, presentation: Presentation, manager: UndoManager, derived: Derived) : Setters {
	const { group } = manager;
	const shallow_copy = function(value: object) {
		return Object.assign(Object.create(null), value);
	};

	const setText = function(id: Id, text: string) {
		const dict: Tree = model.tree.value;
		const node: Node = getNode(id, dict);
		if (node.text !== text) {
			const result: Tree = shallow_copy(dict);
			const item: Node = shallow_copy(node);
			item.text = text;
			setNode(item, id, result);
			group( () => {
				model.tree.value = result;
			}, "set text of item with id " + id, true);
		}
	};

	const setScroll = function(scrollTop: number) {
		presentation.listScroll.value = scrollTop;
	};

	const disclosureToggle = function(id: Id) {
		const collapsed = presentation.allCollapsedIds.value.slice(0);
		const index = collapsed.indexOf(id);
		if (index < 0) {
			collapsed.push(id);
			const flattenedIds = derived.flattenedIdsLinear.value;
			const sortFunction = function(a: number, b: number) {
				const A = flattenedIds.indexOf(a);
				const B = flattenedIds.indexOf(b);
				return A - B;
			};
			collapsed.sort(sortFunction);
		} else {
			collapsed.splice(index, 1);
		}
		presentation.allCollapsedIds.value = collapsed;
	};

	const addItem = function() {
		const parentId = presentation.selectedId.value || 0;
		addChildToParentWithId(parentId);
	};

	const addChildToParentWithId = function(parentId: number) {
		const result = shallow_copy(model.tree.value);
		const childId = model.nextId;
		model.nextId = incrementId(childId);
		const parentNode: Node = getNode(parentId, result);
		const string = textFromId(childId);
		const parentNodeNext: Node = shallow_copy(parentNode);
		parentNodeNext.childIds = [...parentNode.childIds, childId];
		setNode(parentNodeNext, parentId, result);
		const childNode: Node = {
			text: string,
			childIds:[]
		};
		setNode(childNode, childId, result);
		
		const collapsed = presentation.allCollapsedIds.value.slice(0);
		let superId = parentId;
		while (superId) { // expand if inserted item is hidden by disclosure. Expects root node to be 0
			const index = collapsed.indexOf(superId);
			if (index !== -1) {
				collapsed.splice(index,1);
			}
			superId = derived.parentIds.value[superId];
		}

		group( () => {
			model.tree.value = result;
			presentation.selectedId.value = childId;
			presentation.allCollapsedIds.value = collapsed;
		}, "add child to parent with id " + parentId);
	};

	const deleteSelectedItem = function(andReparentChildren: boolean) {
		const id = presentation.selectedId.value;
		if (id > 0) { // can't delete top-level node
			let description = "delete item with id " + id;
			if (andReparentChildren) description += " and reparent children";
			else description += " and all descendants";
			group( () => {
				deleteItemWithId(id, andReparentChildren);
				presentation.selectedId.value = 0;
			}, description);
		}
	};

	const deleteItemWithId = function(id: Id, andReparentChildren: boolean) {
		const tree = shallow_copy(model.tree.value);
		const allCollapsedIds = [...presentation.allCollapsedIds.value];
		const parentId = derived.parentIds.value[id];
		if (andReparentChildren) {
			deleteItemAndReparentChildren(id, parentId, tree, allCollapsedIds);
		} else { // Delete orphaned nodes
			deleteItemAndDescendants(id, parentId, tree, allCollapsedIds);
		}
		model.tree.value = tree; // lol
		presentation.allCollapsedIds.value = allCollapsedIds;
	};

	const deleteItemAndReparentChildren = function(id: Id, topParentId: Id, result: Tree, allCollapsedIds: Array<Id>) { // not recursive // mutates
		const node = getNode(id, result);
		const topParent: Node = getNode(topParentId, result);
		const topParentNode: Node = shallow_copy(topParent);
		const parentChildIds = [ ...topParentNode.childIds];
		const subIndex = parentChildIds.indexOf(id);
		parentChildIds.splice(subIndex, 1, ...node.childIds);
		topParentNode.childIds = parentChildIds;
		setNode(topParentNode, topParentId, result);
		deleteNode(id, result);
		const collapsedIndex = allCollapsedIds.indexOf(id);
		if (collapsedIndex > -1) allCollapsedIds.splice(collapsedIndex,1);
	};

	const deleteItemAndDescendants = function(id: Id, topParentId: Id, result: Tree, allCollapsedIds: Array<Id>) { // recursive // mutates
		const node = getNode(id, result);
		const childIds: Array<number> = node.childIds;
		childIds.forEach( childId => {
			deleteItemAndDescendants(childId, topParentId, result, allCollapsedIds);
		});
		const actualParentId = derived.parentIds.value[id];
		if (actualParentId === topParentId) {
			const topParent: Node = getNode(topParentId, result);
			const topParentNode: Node = shallow_copy(topParent);
			const parentChildIds = [ ...topParentNode.childIds];
			const subIndex = parentChildIds.indexOf(id);
			parentChildIds.splice(subIndex,1);
			topParentNode.childIds = parentChildIds;
			setNode(topParentNode, topParentId, result);
		}
		deleteNode(id, result);
		const collapsedIndex = allCollapsedIds.indexOf(id);
		if (collapsedIndex > -1) allCollapsedIds.splice(collapsedIndex,1);
	};

	const deleteAndReparent = function() {
		deleteSelectedItem(true);
	};

	const deleteAndAllDescendants = function() {
		deleteSelectedItem(false);
	};

	const selectListItem = function(id: Id) {
		presentation.selectedId.value = id;
	};

	const deselect = function() {
		presentation.selectedId.value = 0;
	};

	return {
		setText,
		setScroll,
		disclosureToggle,
		addItem,
		deleteAndReparent,
		deleteAndAllDescendants,
		selectListItem,
		deselect
	};
};

const initDerived = function(model: Model, presentation: Presentation) : Derived {
	const flattenedIdsLinear = computed(() => {
		const dict = model.tree.value;
		const populateFlattenedIdsLinear = function(nodeId: Id, destination: Array<number>) { // Does not include the root node
			const node = getNode(nodeId, dict);
			const childIds = node.childIds || [];
			const childrenLength = childIds.length;
			for (let childIndex=0; childIndex<childrenLength; childIndex++) {
				const childId = childIds[childIndex];
				const childNode = dict[childId];
				if (childNode) {
					destination.push(childId);
					populateFlattenedIdsLinear(childId, destination);
				}
			}
		};
		const result: Array<number> = [];
		populateFlattenedIdsLinear(0, result);
		return result;
	});

	const flattenedDepthLookup = computed(() => {
		const dict = model.tree.value;
		const populateFlattenedDepthLookup = function(nodeId: Id, depth: number, destination: Array<number>) { // Does not include the root node
			if (nodeId !== 0) destination.push(depth);
			const node = getNode(nodeId, dict);
			const childIds = node.childIds || [];
			const childrenLength = childIds.length;
			for (let childIndex=0; childIndex<childrenLength; childIndex++) {
				const childId = childIds[childIndex];
				const childNode = getNode(childId, dict);
				if (childNode) {
					populateFlattenedDepthLookup(childId, depth+1, destination);
				}
			}
		};
		const result: Array<number> = [];
		populateFlattenedDepthLookup(0, 0, result);
		return result;
	});

	const flattenedVisibleIds = computed(() => {
		const dict = model.tree.value;
		const flattenedIds = flattenedIdsLinear.value;
		const collapsed = presentation.allCollapsedIds.value;
		const result: Array<number> = [];
		populateVisible(result,dict,0,flattenedIds,collapsed,0,false);
		return result;
	});

	const populateVisible = function(
		destination: Array<number>,
		dict: Tree,
		nodeId: Id,
		flattenedIds: Array<number>,
		allCollapsedIds: Array<number>,
		collapsedIndex: number,
		ancestorCollapsed: boolean
	) {
		const node = getNode(nodeId, dict);
		const childIds = node.childIds || [];
		const childrenLength = childIds.length;
		const collapsedLength = allCollapsedIds.length;
		for (let childIndex = 0; childIndex < childrenLength; childIndex++) {
			const childId = childIds[childIndex];
			const childNode = getNode(childId, dict);
			if (childNode) {
				let childCollapsed = false;
				while (collapsedIndex < collapsedLength) {
					const collapsedId = allCollapsedIds[collapsedIndex];
					const sortFunction = function(a: number, b: number) {
						const A = flattenedIds.indexOf(a);
						const B = flattenedIds.indexOf(b);
						return A - B;
					};
					const order = sortFunction(childId, collapsedId);
					if (order === 0) {
						childCollapsed = true;
						break;
					} else if (order < 0) break;
					collapsedIndex++;
				}
				if (!ancestorCollapsed) destination.push(childId);
				populateVisible(
					destination,
					dict,
					childId,
					flattenedIds,
					allCollapsedIds,
					collapsedIndex,
					ancestorCollapsed || childCollapsed
				);
			}
		}
	};

	const visibleCollapsedIds = computed(() => {
		const flattenedIds = flattenedIdsLinear.value;
		const visibleIds = flattenedVisibleIds.value;
		const collapsedIds = presentation.allCollapsedIds.value;
		const flattenedLength = flattenedIds.length;
		const visibleLength = visibleIds.length;
		const collapsedLength = collapsedIds.length;
		let visibleIndex = 0;
		let collapsedIndex = 0;
		const result: Array<number> = [];
		for (let flattenedIndex=0; flattenedIndex<flattenedLength; flattenedIndex++) {
			if (collapsedIndex >= collapsedLength || visibleIndex >= visibleLength) break;
			const flattenedId = flattenedIds[flattenedIndex];
			const visibleId = visibleIds[visibleIndex];
			const collapsedId = collapsedIds[collapsedIndex];
			if (flattenedId === collapsedId && flattenedId === visibleId) {
				result.push(collapsedId);
			}
			if (flattenedId === visibleId) visibleIndex++;
			if (collapsedId === flattenedId) collapsedIndex++;
		}
		return result;
	});

	const parentIds = computed(() => {
		const dict = model.tree.value;
		const ids = Object.keys(dict);
		const idsLength = ids.length;
		const result: Record<number, number> = Object.create(null);
		for (let i=0; i<idsLength; i++) {
			const string = ids[i];
			const number = Number(string);
			const item = dict[number];
			const childIds = item.childIds;
			const childLength = childIds.length;
			for (let j=0; j<childLength; j++) {
				const childId = childIds[j];
				result[childId] = number;
			}
		}
		return result;
	});
	return {
		flattenedIdsLinear,
		flattenedDepthLookup,
		flattenedVisibleIds,
		visibleCollapsedIds,
		parentIds
	};
};

const initGetters = function(model: Model, presentation: Presentation, derived: Derived) {
	const numberOfItems = function() {
		return derived.flattenedVisibleIds.value.length;
	};
	const idAtIndex = function(index: number) : Id {
		const length = numberOfItems();
		if (index < 0 || index >= length) return 0;
		return derived.flattenedVisibleIds.value[index];
	};
	const itemAtIndex = function(index: number) : Node {
		const id: Id = idAtIndex(index);
		const dict = model.tree.value;
		return dict[id];
	};
	const depthAtIndex = function(index: number) : number {
		const id: Id = idAtIndex(index);
		if (id <= 0) return 0;
		const absoluteIndex = derived.flattenedIdsLinear.value.indexOf(id);
		if (absoluteIndex < 0) return 0;
		return derived.flattenedDepthLookup.value[absoluteIndex];
	};
	const isExpandableAtIndex = function(index: number) : boolean {
		const item = itemAtIndex(index);
		return item.childIds && item.childIds.length > 0;
	};
	const isExpandedAtIndex = function(index: number) : boolean {
		const id = idAtIndex(index);
		return presentation.allCollapsedIds.value.indexOf(id) > -1;
	};
	const isSelectedAtIndex = function(index: number) : boolean {
		const id = idAtIndex(index);
		return presentation.selectedId.value === id;
	};
	const textAtIndex = function(index: number) : string {
		const item = itemAtIndex(index);
		return item.text;
	};
	const hasSelection = function() {
		return presentation.selectedId.value > 0;
	};
	const getScroll = function() {
		return presentation.listScroll.value;
	};
	
	return {
		numberOfItems,
		hasSelection,
		getScroll,
		idAtIndex,
		depthAtIndex,
		textAtIndex,
		isExpandableAtIndex,
		isExpandedAtIndex,
		isSelectedAtIndex
	};
};

const initState = function(manager: UndoManager, length: number | null | undefined) {
	if (!length || length < 0) length = 0;
	const { undoable, preservable } = manager;
	const model: Model = {
		tree: undoable( randomTree(length) ),
		nextId: length + 1 // lol
	};
	const presentation: Presentation = {
		allCollapsedIds: preservable( [] ),
		selectedId: preservable(0),
		listScroll: preservable(0)
	};
	const derived = initDerived(model, presentation);
	const getters = initGetters(model, presentation, derived);
	const setters = initSetters(model, presentation, manager, derived);
	
	return {
		getters,
		setters,
		model, // for testing
		presentation, // for testing
		derived // for testing
	};
};

export {
	initState,
	State,
	Model,
	Presentation,
	Getters,
	Setters,
	Derived,
	Tree,
	Node,
	Id
};