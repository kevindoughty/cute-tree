import { assert } from "chai";

function isFunction(w) {
	return w && {}.toString.call(w) === "[object Function]";
}

function exists(w) {
	return typeof w !== "undefined" && w !== null;
}

import { UndoManager } from "@kvndy/undo-manager";
import { initState } from "../temp/state.js";

describe("Test", function() {
	it("test", function() {
		assert(isFunction(function() {}));
		assert(isFunction(() => {}));
		assert(!isFunction({}));
		assert(!isFunction("[object Function]"));
		assert(isFunction(exists));
		assert(exists({}));
		assert(exists(false));
		assert(exists(0));
	});

	it("delete", function() {
		const manager = new UndoManager();
		const length = 0;
		const state = initState(manager, length);
		const {
			getters,
			setters,
			model,
			presentation,
			derived
		} = state;
		const {
			flattenedIdsLinear,
			flattenedDepthLookup,
			flattenedVisibleIds,
			visibleCollapsedIds,
			parentIds
		} = derived;
		const {
			setText,
			setScroll,
			disclosureToggle,
			addItem,
			deleteAndReparent,
			deleteAndAllDescendants,
			selectListItem,
			deselect
		} = setters;
		const {
			numberOfItems,
			hasSelection,
			getScroll,
			idAtIndex,
			depthAtIndex,
			textAtIndex,
			isExpandableAtIndex,
			isExpandedAtIndex,
			isSelectedAtIndex
		} = getters;
		const { allCollapsedIds, selectedId, listScroll } = presentation;
		const { tree, nextId } = model;

		const zero = {
			"0":{"text":"zero","childIds":[]}
		};
		assert.deepEqual(tree.value, zero);
		assert.deepEqual(presentation.selectedId.value, 0);
		assert.deepEqual(flattenedIdsLinear.value, []);
		assert.deepEqual(flattenedDepthLookup.value, []);
		assert.deepEqual(flattenedVisibleIds.value, []);
		assert.deepEqual(visibleCollapsedIds.value, []);
		assert.deepEqual(parentIds.value, { });

		addItem();
		const one = {
			"0":{"text":"zero","childIds":[1]},
			"1":{"text":"one","childIds":[]}
		};
		assert.deepEqual(tree.value, one);
		assert.deepEqual(presentation.selectedId.value, 1);
		assert.deepEqual(flattenedIdsLinear.value, [1]); //
		assert.deepEqual(flattenedDepthLookup.value, [1]);
		assert.deepEqual(flattenedVisibleIds.value, [1]);
		assert.deepEqual(visibleCollapsedIds.value, []);
		assert.deepEqual(parentIds.value, { "1":0 });

		addItem();
		const two = {
			"0":{"text":"zero","childIds":[1]},
			"1":{"text":"one","childIds":[2]},
			"2":{"text":"two","childIds":[]}
		};
		assert.deepEqual(tree.value, two);
		assert.deepEqual(presentation.selectedId.value, 2);
		assert.deepEqual(flattenedIdsLinear.value, [1,2]);
		assert.deepEqual(flattenedDepthLookup.value, [1,2]);
		assert.deepEqual(flattenedVisibleIds.value, [1,2]);
		assert.deepEqual(visibleCollapsedIds.value, []);
		assert.deepEqual(parentIds.value, { "1":0, "2":1 });

		deselect();
		addItem();
		const three = {
			"0":{"text":"zero","childIds":[1,3]},
			"1":{"text":"one","childIds":[2]},
			"2":{"text":"two","childIds":[]},
			"3":{"text":"three","childIds":[]}
		};
		assert.deepEqual(tree.value, three);
		assert.deepEqual(presentation.selectedId.value, 3);
		assert.deepEqual(flattenedIdsLinear.value, [1,2,3]);
		assert.deepEqual(flattenedDepthLookup.value, [1,2,1]);
		assert.deepEqual(flattenedVisibleIds.value, [1,2,3]);
		assert.deepEqual(visibleCollapsedIds.value, []);
		assert.deepEqual(parentIds.value, { "1":0, "2":1, "3":0 });

		addItem();
		const four = {
			"0":{"text":"zero","childIds":[1,3]},
			"1":{"text":"one","childIds":[2]},
			"2":{"text":"two","childIds":[]},
			"3":{"text":"three","childIds":[4]},
			"4":{"text":"four","childIds":[]}
		};
		assert.deepEqual(tree.value, four);
		assert.deepEqual(presentation.selectedId.value, 4);
		assert.deepEqual(flattenedIdsLinear.value, [1,2,3,4]);
		assert.deepEqual(flattenedDepthLookup.value, [1,2,1,2]);
		assert.deepEqual(flattenedVisibleIds.value, [1,2,3,4]);
		assert.deepEqual(visibleCollapsedIds.value, []);
		assert.deepEqual(parentIds.value, { "1":0, "2":1, "3":0, "4":3 });
		
		disclosureToggle(1);
		disclosureToggle(3);
		assert.deepEqual(flattenedIdsLinear.value, [1,2,3,4]);
		assert.deepEqual(flattenedDepthLookup.value, [1,2,1,2]);
		assert.deepEqual(flattenedVisibleIds.value, [1,3]);
		assert.deepEqual(visibleCollapsedIds.value, [1,3]);
		assert.deepEqual(parentIds.value, { "1":0, "2":1, "3":0, "4":3 });
		
		selectListItem(1);
		deleteAndAllDescendants();
		const five = {
			"0":{"text":"zero","childIds":[3]},
			"3":{"text":"three","childIds":[4]},
			"4":{"text":"four","childIds":[]}
		};
		assert.deepEqual(tree.value, five);
		assert.deepEqual(presentation.selectedId.value, 0);
		assert.deepEqual(flattenedIdsLinear.value, [3,4]);
		assert.deepEqual(flattenedDepthLookup.value, [1,2]);
		assert.deepEqual(flattenedVisibleIds.value, [3]);
		assert.deepEqual(visibleCollapsedIds.value, [3]);
		assert.deepEqual(parentIds.value, { "3":0, "4":3 });

		selectListItem(3);
		deleteAndReparent();
		const six = {
			"0":{"text":"zero","childIds":[4]},
			"4":{"text":"four","childIds":[]}
		};
		assert.deepEqual(tree.value, six);
		assert.deepEqual(presentation.selectedId.value, 0);
		assert.deepEqual(flattenedIdsLinear.value, [4]);
		assert.deepEqual(flattenedDepthLookup.value, [1]);
		assert.deepEqual(flattenedVisibleIds.value, [4]);
		assert.deepEqual(visibleCollapsedIds.value, []);
		assert.deepEqual(parentIds.value, { "4":0 });

	});

});